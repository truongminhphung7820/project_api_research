<?php

namespace App\Http\Controllers\API;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;

class ProductController extends Controller
{
    protected $productRepo;
    public function __construct(ProductRepositoryInterface $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    /**
     * @OA\Get(
     *     path="/api/products",
     *     tags={"products"},
     *     summary="Get all products for REST API",
     *     description="Multiple status values can be provided with comma separated string",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="perPage",
     *         in="query",
     *         description="Per page count",
     *         required=false,
     *         explode=true,
     *         @OA\Schema(
     *             default="10",
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid status value"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error"
     *     )
     * )
     */
    public function index()
    {
        try {
            $data = $this->productRepo->getAllProduct();
            return response()->json(['success' => true, 'data' => new ProductResource($data)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation  = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required'
            ], [
                'name.required' => 'Name là bắt buộc !',
                'slug.required' => 'Slug là bắt buộc !',
            ]);
            if ($validation->fails()) {
                return response()->json(['success' => false, 'data' => $validation->errors()]);
            }
            $this->productRepo->create($request->all());
            return response()->json(['success' => true, 'data' => 'create successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = $this->productRepo->getById($id);
            return response()->json(['success' => true, 'data' => new ProductResource($data)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validation  = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required'
            ], [
                'name.required' => 'Name là bắt buộc !',
                'slug.required' => 'Slug là bắt buộc !',
            ]);
            if ($validation->fails()) {
                return response()->json(['success' => false, 'data' => $validation->errors()]);
            }
            $this->productRepo->update($id, $request->all());
            return response()->json(['success' => true, 'data' => 'update successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->productRepo->delete($id);
            return response()->json(['success' => true, 'data' => 'delete successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }
}
