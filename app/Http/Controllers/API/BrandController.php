<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\BrandRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Resources\Brand as BrandResouce;

class BrandController extends Controller
{
    protected $brandRepo;
    public function __construct(BrandRepositoryInterface $brandRepo)
    {
        $this->brandRepo = $brandRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = $this->brandRepo->getAllBrand();
            return response()->json(['success' => true, 'data' => new BrandResouce($data)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation  = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'slug'=>'required'
            ], [
                'name.required' => 'Name là bắt buộc !',
                'slug.required' => 'Slug là bắt buộc !',
            ]);
            if ($validation->fails()) {
                return response()->json(['success' => false, 'data' => $validation->errors()]);
            }
            $this->brandRepo->create($request->all());
            return response()->json(['success' => true, 'data' => 'create successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = $this->brandRepo->getById($id);
            return response()->json(['success' => true, 'data' => new BrandResouce($data)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validation  = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'slug'=>'required'
            ], [
                'name.required' => 'Name là bắt buộc !',
                'slug.required' => 'Slug là bắt buộc !',
            ]);
            if ($validation->fails()) {
                return response()->json(['success' => false, 'data' => $validation->errors()]);
            }
            $this->brandRepo->update($id, $request->all());
            return response()->json(['success' => true, 'data' => 'update successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->brandRepo->delete($id);
            return response()->json(['success' => true, 'data' => 'delete successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }
}
