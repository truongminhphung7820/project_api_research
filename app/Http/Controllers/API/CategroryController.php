<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CategroryRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Resources\Categrory as CategroryResource;

class CategroryController extends Controller
{
    protected $cateRepo;
    public function __construct(CategroryRepositoryInterface $cateRepo)
    {
        $this->cateRepo = $cateRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = $this->cateRepo->getAllCate();
            foreach ($data as $key => $value) {
                $str = '';
                for ($i = 0; $i < $value->level; $i++) {
                    echo $str;
                    $str .= '--';
                }
                echo $value->name . "<br>";
            }
            // return response()->json(['success' => true, 'data' => new CategroryResource($data)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation  = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required'
            ], [
                'name.required' => 'Name là bắt buộc !',
                'slug.required' => 'Slug là bắt buộc !',
            ]);
            if ($validation->fails()) {
                return response()->json(['success' => false, 'data' => $validation->errors()]);
            }
            $this->cateRepo->create($request->all());
            return response()->json(['success' => true, 'data' => 'create successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = $this->cateRepo->getById($id);
            return response()->json(['success' => true, 'data' =>  new CategroryResource($data)]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validation  = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'slug' => 'required'
            ], [
                'name.required' => 'Name là bắt buộc !',
                'slug.required' => 'Slug là bắt buộc !',
            ]);
            if ($validation->fails()) {
                return response()->json(['success' => false, 'data' => $validation->errors()]);
            }
            $this->cateRepo->update($id, $request->all());
            return response()->json(['success' => true, 'data' => 'update successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->cateRepo->delete($id);
            return response()->json(['success' => true, 'data' => 'delete successfully']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }
}
