<?php
namespace App\Repositories;

use App\Repositories\Interfaces\BrandRepositoryInterface;

class BrandRepository extends BaseRepository implements BrandRepositoryInterface{
    public function getModel()
    {
        return \App\Models\Brand::class;
    }
    public function getAllBrand()
    {
        return $this->model->with('products','categrories')->get();
    }
    public function getById($id)
    {
        return $this->model->with('products','categrories')->findOrFail($id);
    }
}
?>