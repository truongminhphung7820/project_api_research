<?php
namespace App\Repositories\Interfaces;

interface BrandRepositoryInterface extends RepositoryInterface{
    public function getAllBrand();
    public function getById($id);
}
?>