<?php
namespace App\Repositories\Interfaces;

interface CategroryRepositoryInterface extends RepositoryInterface{
    public function getAllCate();
    public function getById($id);
}
?>