<?php
namespace app\Repositories\Interfaces;

interface RepositoryInterface
{
    public function all();
    public function find($id);
    public function create($attributes = []);
    public function save(array $inputs, $id = null);
    public function update($id, $attributes = []);
    public function delete($id);
    
}