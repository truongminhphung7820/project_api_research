<?php
namespace App\Repositories\Interfaces;

interface ProductRepositoryInterface extends RepositoryInterface{
    public function getAllProduct();
    public function getById($id);
}
 ?>