<?php
namespace App\Repositories;
use App\Repositories\BaseRepository;
use App\Repositories\Interfaces\CategroryRepositoryInterface;

class CategroryRepository extends BaseRepository implements CategroryRepositoryInterface{
    public function getModel()
    {
        return \App\Models\Categrory::class;
    }
    public function getAllCate()
    {
       $categrories = $this->model->with('products','brands')->get();
       $listCategrogy = [];
       $this->model::recursive($categrories, $parents = 0, $level = 1, $listCategrogy);
       return $listCategrogy;
    }
    public function getById($id)
    {
        return $this->model->with('products','brands')->findOrFail($id);
    }
}
?>