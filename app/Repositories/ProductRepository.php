<?php
namespace App\Repositories;

use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface{

    public function getModel()
    {
        return \App\Models\Product::class;
    }
    public function getAllProduct()
    {
        return $this->model->with('categrory','brand')->get();
    }
    public function getById($id)
    {
        return $this->model->with('categrory','brand')->findOrFail($id);
    }
}
