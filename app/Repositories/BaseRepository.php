<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;;

abstract class BaseRepository implements RepositoryInterface
{

    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    public function setModel()
    {
        $this->model = app()->make($this->getModel());
    }
    
    abstract public function getModel();

    public function all()
    {
        return $this->model->all();
    }
    
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function save(array $inputs, $id = null)
    {
        return $this->model->updateOrCreate(['id' => $id], $inputs);
    }
    
    public function update($id, $attributes = [])
    {
        $resulst = $this->model->findOrFail($id);
        if ($resulst)
        {
            return $resulst->update($attributes);
        }

        return false;
    }

    public function delete($id)
    {
        $resulst = $this->model->findOrFail($id);
        if ($resulst)
        {
            return $resulst->delete();
        }

        return false;
    }
}
