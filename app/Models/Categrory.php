<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categrory extends Model
{
    use HasFactory;
    protected $table = "categrories";
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'parent_id',
        'name',
        'slug',
        'image',
        'der',
        'created_at',
        'updated_at'
    ];
    public function products()
    {
        return $this->hasMany(Product::class, 'cate_id', 'id');
    }
    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'brand_categrory', 'cate_id', 'brand_id');
    }

    public static function recursive($categrories, $parents = 0, $level = 1, &$listCategrogy)
    {
        if (count($categrories) > 0) {
            foreach ($categrories as $key => $value) {
                if ($value->parent_id == $parents) {
                    $value->level = $level;
                    $listCategrogy[] = $value;
                    unset($categrories[$key]);
                    $parent = $value->id;
                    self::recursive($categrories, $parent, $level + 1, $listCategrogy);
                }
            }
        }
    }
}
