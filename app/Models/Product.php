<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "products";
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'brand_id',
        'cate_id',
        'name',
        'slug',
        'image',
        'price',
        'der',
        'created_at',
        'updated_at'
    ];
    public function brand()
    {
       return $this->belongsTo(Brand::class,'brand_id','id');
    }
    public function categrory()
    {
        return $this->belongsTo(Categrory::class,'cate_id','id');
    }
}
