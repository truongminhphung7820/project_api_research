<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $table = "brands";
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'name',
        'slug',
        'image',
        'der',
        'created_at',
        'updated_at'
    ];

    public function products()
    {
        return $this->hasMany(Product::class,'brand_id','id');
    }
    public function categrories()
    {
        return $this->belongsToMany(Categrory::class,'brand_categrory','brand_id','cate_id');
    }
}
