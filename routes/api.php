<?php

use App\Http\Controllers\API\BrandController;
use App\Http\Controllers\API\CategroryController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\UserController;
use App\Models\Brand;
use App\Models\Categrory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use L5Swagger\Annotations as SWG;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('products',ProductController::class);
Route::resource('brands',BrandController::class);
Route::resource('categrories',CategroryController::class);
Route::resource('users',UserController::class);
